# Simple Linear Regression

In this simple project you can find an example of implementation on Python
and the intuition behind the Linear Regression Model.


## Intuition

The Linear Regression Model can be considered one of the most simple of Regression Model.

### y = b + ax

Through the equation above we can define our model. It means, you have a dependent variable (y),
a constant (b), an independent variable (x)  and a coefficient (a).
The value of coefficient a should be close to the connection between x and y.

